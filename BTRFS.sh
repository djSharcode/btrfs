#!/bin/bash  
setfont ter-124b 
lsblk -do name,size -e 7,11 ; printf "Device name: " ; read -r D ; E="/dev/${D}" ; sgdisk ${E} -Z -o -n 1::+512M -t 1:EF00 -n -i -v -p 
E1="$(ls /dev/* | grep -E "^${E}p?1$")" ; E2="$(ls /dev/* | grep -E "^${E}p?2$")" ; mkfs.vfat "${E1}" ; mkfs.btrfs -fq "${E2}"
F='btrfs su cr @' ; G='mount -o noatime,compress=zstd,discard=async,subvol=@' ; mount "${E2}" /mnt ; cd /mnt ; ${F} 
${F}home ; cd ; umount /mnt ; ${G} "${E2}" /mnt ; mkdir /mnt/{boot,home} ; ${G}home "${E2}" /mnt/home ; mount "${E1}" /mnt/boot
